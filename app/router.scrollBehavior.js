export default function (to) {
  if (to.hash) {
    return {
      el: to.hash
    }
  }
}
