const config = {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'dodo-rabota',
    htmlAttrs: {
      lang: 'ru'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
      { name: 'msapplication-TileColor', content: '#da532c' },
      { name: 'theme-color', content: '#ffffff' }
    ],
    link: [
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: '/favicon/apple-touch-icon.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/favicon/favicon-32x32.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/favicon/favicon-16x16.png'
      },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon/favicon.ico' },
      { rel: 'manifest', href: '/favicon/site.webmanifest' }
    ],
    script: [
      {
        type: 'text/javascript',
        src: '/js/swiper-bundle.min.js',
        body: true
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['normalize.css/normalize.css', '@/assets/styles/main.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    '@nuxtjs/style-resources',
    '@nuxt/image',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/proxy',
    '@nuxtjs/axios',
    '@nuxt/image'
  ],

  purgecss: {
    enabled: true // Always enable purgecss
    // safelist: ['my-class'], // Add my-class token to the safelist (e.g. .my-class)
  },

  image: {
    provider: 'imageengine'
  },

  render: {
    bundleRenderer: {
      shouldPrefetch: () => false,
      shouldPreload: (fileWithoutQuery, asType) =>
        ['script', 'style', 'font'].includes(asType)
    }
  },

  bundleRenderer: {
    shouldPrefetch: () => false,
    shouldPreload: (file, type) => {
      return ['script', 'style', 'font'].includes(type)
    }
  },

  styleResources: {
    scss: [
      '@/assets/styles/_variables.scss',
      '@/assets/styles/breakpoints.scss'
    ]
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    proxy: !!process.env.PROXY_URL
  },

  proxy: process.env.PROXY_URL
    ? {
        '/api/': process.env.PROXY_URL || '',
        '/uploads/': process.env.PROXY_URL || ''
      }
    : {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    // extractCSS: true,
    optimization: {
      runtimeChunk: 'single',
      splitChunks: {
        chunks: 'all',
        maxInitialRequests: Infinity,
        minSize: 0,
        cacheGroups: {
          styles: {
            name: 'styles',
            test: /\.(css|vue)$/,
            chunks: 'all',
            enforce: true
          },
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name(module) {
              const packageName = module.context.match(
                /[\\/]node_modules[\\/](.*?)([\\/]|$)/
              )[1]
              return `npm.${packageName.replace('@', '')}`
            }
          }
        }
      }
    },
    hardSource: true,
    app: ({ isDev, isModern }) =>
      isDev
        ? `[name]${isModern ? '.modern' : ''}.js`
        : `[contenthash:7]${isModern ? '.modern' : ''}.js`,
    chunk: ({ isDev }) => (isDev ? '[name].js' : '[id].[contenthash].js')
  },

  publicRuntimeConfig: {
    langLocale: process.env.LANG_LOCALE || 'ru'
  }
}

export default config
